build:
	rm -rf dist/NvStreamingSdkCore.xcframework
	rm -rf temp
	mkdir temp
	mkdir temp/iphonesimulator temp/iphoneos
	cp -r lib/ios/NvStreamingSdkCore.framework temp/iphoneos/NvStreamingSdkCore.framework
	cp -r lib/ios/iphonesimulator/NvStreamingSdkCore.framework temp/iphonesimulator/NvStreamingSdkCore.framework
	cp -r include/sdkcore/ios/* temp/iphonesimulator/NvStreamingSdkCore.framework/Headers
	xcodebuild -create-xcframework \
		-framework temp/iphoneos/NvStreamingSdkCore.framework \
		-framework temp/iphonesimulator/NvStreamingSdkCore.framework \
		-output dist/NvStreamingSdkCore.xcframework
		#TODO: https://franki.atlassian.net/browse/FRNKI-440 add the dsym from the framework
		#-debug-symbols $(abspath $(CURDIR))/lib/ios/NvStreamingSdkCore.framework.dSYM \
		-framework temp/iphonesimulator/NvStreamingSdkCore.framework \
		-output dist/NvStreamingSdkCore.xcframework

	rm -rf temp
