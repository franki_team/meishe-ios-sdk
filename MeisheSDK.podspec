Pod::Spec.new do |spec|
  spec.name         = "MeisheSDK"
  spec.version      = "3.11.1"
  spec.summary      = "Placeholder for Meishe iOS SDK"

  spec.description  = <<-DESC
  Placeholder for Meishe iOS SDK. 
  Check https://www.meishesdk.com/new/doc
                   DESC

  spec.homepage     = "https://bitbucket.org/franki_team/meishe-ios-sdk"

  spec.license      = "MIT"

  spec.author       = { "Tyler Yang" => "tyler.yang@befranki.com" }
  spec.ios.deployment_target  = '13.0'
  spec.source       = { :git => "https://bitbucket.org/franki_team/meishe-ios-sdk.git", :tag => "#{spec.version}" }
  
  headers_path = "dist/NvStreamingSdkCore.xcframework/NvStreamingSdkCore.framework/Headers/*.h"
  spec.source_files = headers_path
  spec.public_header_files = headers_path
  spec.vendored_frameworks = "dist/NvStreamingSdkCore.xcframework"
  spec.xcconfig = {"ENABLE_BITCODE" => "NO"}
end
