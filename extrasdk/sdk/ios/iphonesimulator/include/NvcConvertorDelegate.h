//================================================================================
//
// (c) Copyright China Digital Video (Beijing) Limited, 2016. All rights reserved.
//
// This code and information is provided "as is" without warranty of any kind,
// either expressed or implied, including but not limited to the implied
// warranties of merchantability and/or fitness for a particular purpose.
//
//--------------------------------------------------------------------------------
//   Birth Date:    June 28. 2017
//   Author:        NewAuto video team
//================================================================================

#ifndef NvcConvertorDelegate_h
#define NvcConvertorDelegate_h

#import <Foundation/Foundation.h>

/*! \if ENGLISH
 *  \brief Video conversion delegate
 *
 *  In the conversion progress, the conversion progress and the final conversion result can be displayed via proxy.
 *  \else
 *  \brief 视频文件转码代理
 *
 *  在转码过程中可通过代理显示转码进度及转码结果。
 *  \endif
*/
@protocol NvcConvertorDelegate <NSObject>
@optional

/*! \if ENGLISH
 *  \brief Conversion succeeds
 *  \else
 *  \brief 转码完成
 *  \endif
*/
- (void)convertFinished;

/*! \if ENGLISH
 *  \brief Conversion fails
 *  \param error Error value
 *  \else
 *  \brief 转码失败
 *  \param error 错误值
 *  \endif
*/
- (void)convertFaild:(NSError *)error;

/*! \if ENGLISH
 *  \brief Conversion progress
 *  \param progress Current progress
 *  \else
 *  \brief 转码进度
 *  \param progress 当前进度值
 *  \endif
*/
- (void)convertProgress:(int)progress;
@end

#endif /* NvcConvertorDelegate_h */
