//================================================================================
//
// (c) Copyright China Digital Video (Beijing) Limited, 2016. All rights reserved.
//
// This code and information is provided "as is" without warranty of any kind,
// either expressed or implied, including but not limited to the implied
// warranties of merchantability and/or fitness for a particular purpose.
//
//--------------------------------------------------------------------------------
//   Birth Date:    June 28. 2017
//   Author:        NewAuto video team
//================================================================================
#ifndef NvcConvertorBaseDef_h
#define NvcConvertorBaseDef_h

#include <stdint.h>

#define NVC_M_SEGMET_COMPLETE                1   //!< \if ENGLISH Internal use, no explanation \else 注：内部使用，不作解释 \endif
#define NVC_NOERROR                          0   //!< \if ENGLISH No error \else 没有错误 \endif
#define NVC_E_FAIL                           -1  //!< \if ENGLISH Fail \else 失败 \endif
#define NVC_E_FILE_EOF                       -2  //!< \if ENGLISH At the end of file \else 文件末尾 \endif
#define NVC_E_INVALID_POINTER                -3  //!< \if ENGLISH Invalid pointer \else 无效指针 \endif
#define NVC_E_INVALID_PARAMETER              -4  //!< \if ENGLISH Invalid parameter \else 无效参数 \endif
#define NVC_E_NOT_INITIALIZED                -5  //!< \if ENGLISH Not initialized \else 未初始化 \endif
#define NVC_E_NO_VIDEO_STREAM                -6  //!< \if ENGLISH No video stream \else 输入文件不存在视频流 \endif
#define NVC_E_CONVERTOR_IS_OPENED            -7  //!< \if ENGLISH Convertor is opened \else 当前转码器已经打开 \endif
#define NVC_E_CONVERTOR_IS_STARTED           -8  //!< \if ENGLISH Convertor is started \else 正在转码 \endif


// Structure to represent a rational num/den

/*! \if ENGLISH
 *  \brief Proportion
 *  \else
 *  \brief 比例值
 *  \endif
*/
struct SNvcRational {
    int num;     //!< \if ENGLISH Numerator \else 分子 \endif
    int den;     //!< \if ENGLISH Denominator \else  分母\endif
};

/*! \if ENGLISH
 *  \brief The height level of output video
 *  \else
 *  \brief 转码输出视频的分辨率高度级别
 *  \endif
*/
enum ENvcOutputVideoResolution {
    NvcOutputVideoResolution_NotResize = 0, //!< \if ENGLISH Output video, original height \else 原始视频高度输出 \endif
    NvcOutputVideoResolution_360 = 1,       //!< \if ENGLISH Output video, 360P in height \else 输出视频高度-360 \endif
    NvcOutputVideoResolution_480 = 2,       //!< \if ENGLISH Output video, 480P in height \else 输出视频高度-480 \endif
    NvcOutputVideoResolution_720 = 3,       //!< \if ENGLISH Output video, 720P in height \else 输出视频高度-720 \endif
    NvcOutputVideoResolution_1080 = 4,      //!< \if ENGLISH Output video, 1080P in height \else 输出视频高度-1080 \endif
    NvcOutputVideoResolution_90 = 5,       //!< \if ENGLISH Output video, 90P in height \else 输出视频高度-90 \endif
    NvcOutputVideoResolution_108 = 6,       //!< \if ENGLISH Output video, 108P in height \else 输出视频高度-108 \endif
    NvcOutputVideoResolution_180 = 7,       //!< \if ENGLISH Output video, 180P in height \else 输出视频高度-108 \endif
    NvcOutputVideoResolution_270 = 8,       //!< \if ENGLISH Output video, 270P in height \else 输出视频高度-270 \endif
};

//
// output config
//

/*! \if ENGLISH
 *  \brief Output configuration
 *  \else
 *  \brief 输出配置
 *  \endif
*/
#define NVC_OUTPUT_CONFIG_REVERSE   0x1

/*! \if ENGLISH
 *  \brief Video conversion configuration
 *  \else
 *  \brief 视频文件转码参数配置
 *  \endif
*/
struct SNvcOutputConfig {
    enum ENvcOutputVideoResolution videoResolution;  //!< \if ENGLISH Output video height level. \else 转码输出视频高度级别类型 \endif
    float from;                                      //!< \if ENGLISH Conversion starting position (in second). \else 转换起始位置,以秒为单位 \endif
    float to;                                        //!< \if ENGLISH Conversion ending position (in second). \else 转换结束位置,以秒为单位 \endif
    uint64_t dataRate;                               //!< \if ENGLISH Output data rate (in byte). \else 转换后文件的码流,字节为单位 \endif
    int fpsForWebp;                                  //!< \if ENGLISH If the outputs is in WebP format, frame rate needs to be set, which ranges from 1 to 25. \else  如果输出为WebP动画格式，需要设置帧率．最大25，最小１ \endif
};

#endif /* NvcConvertorBaseDef_h */
