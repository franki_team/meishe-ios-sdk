//================================================================================
//
// (c) Copyright China Digital Video (Beijing) Limited, 2016. All rights reserved.
//
// This code and information is provided "as is" without warranty of any kind,
// either expressed or implied, including but not limited to the implied
// warranties of merchantability and/or fitness for a particular purpose.
//
//--------------------------------------------------------------------------------
//   Birth Date:    June 28. 2017
//   Author:        NewAuto video team
//================================================================================
#pragma once

#include "NvcConvertorBaseDef.h"
#include "NvcConvertorDelegate.h"
#import <Foundation/Foundation.h>

/*! \if ENGLISH
 *  \brief Video file conversion.
 *
 *  Video file conversion is the process of starting a video file，setting the path of the video file to be converted, and then starting the video converting and outputting the converted video file.
 *  The video file conversion divides into forward conversion and reverse conversion with the final video in the mov. and webp. format. In addition, as the conversion processes, the conversion progress and the final conversion result can be displayed via proxy.
 *  \else
 *  \brief 视频文件转码
 *
 *  视频文件转码，是通过设置待转码视频文件的路径，打开视频文件，然后开启视频转码并输出转码的视频文件的过程。视频转码分为正向转码，反向转码。转码的结果是生成.mov或者.webp格式的视频文件。同时在转码过程中可通过代理显示转码进度及转码结果。
 *  \endif
*/
@interface NvcConvertor : NSObject{
    
  __weak id<NvcConvertorDelegate> _delegate;
}

@property(nonatomic, weak) id<NvcConvertorDelegate> delegate; //!< \if ENGLISH Proxy \else 代理 \endif

/*! \if ENGLISH
 *  \brief Installation license
 *  \param licenseFile License file
 *  \return Returns BOOL value. "true" means success, while "false" means failure.
 *  \else
 *  \brief 安装许可
 *  \param licenseFile 许可文件
 *  \return 返回BOOL值。值为true,则安装成功，否则安装失败。
 *  \endif
*/
+ (BOOL)InstallLicense:(NSData*)licenseFile;

/*! \if ENGLISH
 *  \brief Initializes by the maximum of caches.
 *  \param maxCacheCount Maximum cache count
 *  \return Returns instancetype object.
 *  \else
 *  \brief 通过最大缓存数初始化
 *  \param maxCacheCount 最大缓存数
 *  \return 返回instancetype类型，表示返回类对象
 *  \endif
*/
- (instancetype)initWithMaxCacheSample:(NSInteger)maxCacheCount;

/*! \if ENGLISH
 *  \brief Sets the I-frame-only mode
 *  \param iFrameOnly Whether to set I frame
 *  \else
 *  \brief 设置纯I帧
 *  \param iFrameOnly 是否设置纯I帧
 *  \endif
*/
- (void)setIFrameOnly:(BOOL)iFrameOnly;

/*! \if ENGLISH
 *  \brief Opens file to be converted.
 *  \param inputFile Path of input file
 *  \param outputPath Output file. P.S. conversion supports .webp and .mov as output.
 *  \param config Convertion configuration.
 *  \return Returns NSInsger value to describe the state of opening file.
 *  \else
 *  \brief 打开转码视频文件
 *  \param inputFile 待转码文件路径
 *  \param outputPath 要转码输出的文件。注：转码可输出.webp或者.mov文件。
 *  \param config 转码参数配置对象
 *  \return 返回NSInteger值，表示文件打开的状态值。
 *  \endif
*/
- (NSInteger)open:(NSString*)inputFile outputFile:(NSString*)outputPath setting:(struct SNvcOutputConfig*)config;

/*! \if ENGLISH
 *  \brief Closes file.
 *  \else
 *  \brief 文件关闭
 *  \endif
*/
- (void)close;

/*! \if ENGLISH
 *  \brief Starts conversion.
 *  \return Returns NSInteger value to describe the state returned by starting conversion.
 *  \else
 *  \brief 开启转码
 *  \return 返回NSInteger值，表示文件转码开启的状态值。
 *  \endif
*/
- (NSInteger)start;

/*! \if ENGLISH
 *  \brief Stops conversion
 *  \return Returns NSInteger value to describe the state returned by stopping conversion.
 *  \else
 *  \brief 停止转码
 *  \return 返回NSInteger值，表示文件转码停止的状态值。
 *  \endif
*/
- (NSInteger)stop;

/*! \if ENGLISH
 *  \brief Cancels conversion
 *  \else
 *  \brief 取消转码
 *  \endif
*/
- (void)cancel;

/*! \if ENGLISH
 *  \brief Gets the progress of conversion.
 *  \return Returns double value to describe the progress of conversion.
 *  \else
 *  \brief 获取转码进度值
 *  \return 返回double值。表示文件转码进度值。
 *  \endif
*/
- (float)getProgress;

/*! \if ENGLISH
 *  \brief Convertor has started to convert.
 *  \return Returns BOOL value to check whether the convertor has started . If it is "true", conversion has started, otherwise, conversion should be started.
 *  \else
 *  \brief 当前转码器已经开启转码状态
 *  \return 返回BOOL值, 表示转码器是否已经开启转码。值为TRUE, 则转码器正在转码，否则开启转码。
 *  \endif
*/
- (BOOL)IsOpened;

@end


