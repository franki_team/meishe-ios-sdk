//
//  ViewController.swift
//  DemoApp
//
//  Created by Tyler Yang on 29/10/20.
//

import UIKit
import NvStreamingSdkCore

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let window = NvsLiveWindow()!
        view.addSubview(window)
        window.frame = view.frame
        window.backgroundColor = .red
    }


}

